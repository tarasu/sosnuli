module ApplicationHelper


  def devise_mapping
    Devise.mappings[:user]
  end
 
  def resource_name
    devise_mapping.name
  end

  def resource_class
    devise_mapping.to
  end

  def user_name
    current_user.name.present? ? current_user.name : 'Пользователь'
  end
end
