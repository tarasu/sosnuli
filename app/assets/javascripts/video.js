function VideoPreview(video) {
  this.$video = $(video);
  try {
    this.initYouTubeVideo();
  } catch(e) {
    this.$video.css('background', '#ccc');
  }
}

VideoPreview.prototype.initYouTubeVideo = function() {
  var youtubeUrl = this.$video.find('.video').data('youtube'),
      videoId,
      apiUrl,
      imgUrl,
      link,
      title,
      duration;

  if (youtubeUrl) {
    videoId = youtubeUrl.match(/(?:&|\?)v=([^&]+)/);
    apiUrl = 'http://gdata.youtube.com/feeds/api/videos/' + videoId[1] + '?v=2&alt=jsonc';

    if (videoId && videoId[1]) {
      imgUrl = 'http://i2.ytimg.com/vi/' + videoId[1] + '/hqdefault.jpg';
      link = this.$video.find('.video');

      $.get(apiUrl, function(data) {
        title = link.data('title');
        duration = secondsToTime(data.data.duration);
        link.append($('<img src="' + imgUrl + '"/>'));
        link.append($('<span class="clear"><b>' + title + '</b><i>' + duration + '</i></span>'));

        link.click(function() {

        });
      });
    }
  }
};

function initVideoPreviews() {
  $('.sv-video-list-block').each(function() {
    new VideoPreview(this);
  });
}

function initVideoPlayer() {
  $('.sv-video-player').each(function() {
    var $this = $(this),
      youtubeUrl = $this.data('youtube'),
      videoId;

    if (youtubeUrl) {
      videoId = youtubeUrl.match(/(?:&|\?)v=([^&]+)/);

      if (videoId && videoId.length > 0) {
        $this.tubeplayer({
          width: 640,
          height: 350,
          allowFullScreen: 'true',
          initialVideo: videoId[1],
          preferredQuality: 'default'
        });
      }
    }
  });
}


function VideoSearchForm() {
  this.initToggleButton();
  this.initSearchTypeButtons();
}

VideoSearchForm.prototype.initToggleButton = function () {
  $('[data-js=videos-search]').unbind('click').on('click', function(e) {
    e.preventDefault();

    $('.sv-videos-search-form').slideToggle();
    $('#sv-videos-block').find('li.search').find('a').toggleClass('active');
  });
};

VideoSearchForm.prototype.initSearchTypeButtons = function () {
  var $this, $buttons, $field;

  $buttons = $('.sv-videos-search-form__link');
  $field = $('[data-js=video-search-type]');
  $('[data-search-type="' + $field.val() + '"]').addClass('sv-videos-search-form__link_active');

  $buttons.on('click', function (e) {
    e.preventDefault();

    $this = $(this);
    $buttons.removeClass('sv-videos-search-form__link_active');
    $this.addClass('sv-videos-search-form__link_active');
    $field.val($this.data('search-type'));
  });
};

$(document).ready(function() {
  new VideoSearchForm();

  initVideoPreviews();
  initVideoPlayer();

  // $('[data-js=video-list]').infinitescroll({
  //   navSelector: '.sv-videos-hidden-nav',
  //   nextSelector: '.sv-videos-hidden-nav link[rel=next]',
  //   itemSelector: '[data-js=video]',
  //   appendCallback: true
  // }, function () {
  //   initVideoPreviews();
  // });
});
