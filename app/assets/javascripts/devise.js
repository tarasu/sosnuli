$(document).ready(function() {

  $('.so-login').click(function(e) {
    e.preventDefault();
    $('#tab-register').hide();
    $('#tab-forgot-password').hide();
    $('#tab-login').show();
    $('[data-js=link-tab-register]').parent('li').removeClass('active');
    $('[data-js=link-tab-login]').parent('li').addClass('active');
  })

  $('[data-js=link-tab-register]').click(function(e) {
    e.preventDefault();
    $('#tab-login').hide();
    $('#tab-forgot-password').hide();
    $('#tab-register').show();
    $(this).parent('li').addClass('active');
    $(this).parent('li').prev().removeClass('active');
  })

  $('[data-js=link-tab-login]').click(function(e) {
    e.preventDefault();
    $('#tab-register').hide();
    $('#tab-forgot-password').hide();
    $('#tab-login').show();
    $(this).parent('li').addClass('active');
    $(this).parent('li').next().removeClass('active');
  })

  $('[data-js=link-tab-forgot-password]').click(function(e) {
    e.preventDefault();
    $('[data-js=link-tab-login]').parent('li').removeClass('active');
    $('#tab-login').hide();
    $('#tab-forgot-password').show();
  })
});