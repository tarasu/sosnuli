$(function(){

  $(document).on('click','.so-discussion',function(event){
    event.preventDefault();
    return $(this).find('a.so-link-to-discussion').trigger('click');
  });

  $(document).on('click','.so-video',function(event){
    event.preventDefault();
    return $(this).find('a.so-video-link').trigger('click');
  });

  //разделительная линия между видео и обсуждениями

  $('#sv-room-content').css('height',$(window).height() - 210 + 'px');
  $(window).resize(function() {
    if ($('#slide-down-off').is('a')) {
      $('#so-videos-list').css('height', $(window).height() - $('.so-separation-line').height() * 4);
    }
    $('#sv-room-content').css('height',$(window).height() - 210 + 'px');
  })

  $(document).on('click','#slide-down-center',function(event){
    event.preventDefault();
    $('#so-videos-list').animate({height:'62%'}, 500);
    $('#so-discussions-list').animate({height:'100%'}, 500);
    $('#so-discussions-list').show();
    $('html, body').animate({ scrollTop: 0 }, 500);
    $('#so-videos-list').queue(function(next){ 
                      $('body').css('overflow','hidden');
                      $(this).css('overflow','hidden');
                      next(); 
    });
    $('#slide-down-off').attr('id', 'slide-center-down');
    $('#slide-down-center').attr('id', 'slide-center-up');
  });

  $(document).on('click','#slide-center-down',function(event){
    event.preventDefault();
    wSize = $(window).height() - $('.so-separation-line').height() * 4;
    vListSize = 650;

    if (wSize <  vListSize)
      wSize = vListSize;
    $('#so-videos-list').animate({height: wSize}, 500);
    $('#so-videos-list').delay(0)
                        .queue(function(next){ 
                          $('body').css('overflow','visible');
                          $(this).css('overflow','visible'); 
                          $('#so-discussions-list').hide();
                          next(); 
    });
    $('#slide-center-down').attr('id', 'slide-down-off');     
    $('#slide-center-up').attr('id', 'slide-down-center');     
  });

  $(document).on('click','#slide-center-up',function(event){
    event.preventDefault();
    $('#so-discussions-list').delay(10)
                             .queue(function(next){ 
                              $('body').css('overflow','visible');
                              next(); 
    });    
    $('#so-videos-list').animate({height:'0%'}, 500);
    $('#slide-center-up').attr('id', 'slide-up-off');     
    $('#slide-center-down').attr('id', 'slide-up-center');    
  });

  $(document).on('click','#slide-up-center',function(event){
    event.preventDefault();
    $('#so-videos-list').animate({height:'62%'}, 500);
    $('#slide-up-off').attr('id', 'slide-center-up');   
    $('#so-discussions-list').delay(10)
                             .queue(function(next){ 
                                $('body').css('overflow','hidden');
                                $(this).css('height','100%'); 
                                next(); 
                            });    
    $('#slide-up-center').attr('id', 'slide-center-down');    
  });

  $(document).on('click','#slide-up-off',function(event){
    event.preventDefault();
    $('#so-discussions-list').animate({height:'0%'}, 500);
    $('#slide-up-off').attr('id', 'slide-off-bg');     
    $('#slide-up-center').attr('id', 'slide-off-down');    

    $('#so-discussions-list').delay(0)
                        .queue(function(next){ 
                          $(this).hide();
                          next(); 
    });        
  });

  $(document).on('click','#slide-off-down',function(event){
    event.preventDefault();
    $('body').css('overflow','visible');
    // $('#so-discussions-list').animate({height:'780px'}, 500); 
    $('#so-discussions-list').show();
    $('#so-discussions-list').animate({height:'100%'}, 500);
    // $('#so-discussions-list').delay(10)
    //                     .queue(function(next){ 
    //                       $(this).show();
    //                       // $('body').css('overflow','hidden');
    //                       // $(this).css('height','100%'); 
    //                       next(); 
    // });   
    $('#slide-off-bg').attr('id', 'slide-up-off');     
    $('#slide-off-down').attr('id', 'slide-up-center');    
  });

});