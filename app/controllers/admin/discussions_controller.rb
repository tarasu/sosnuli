class Admin::DiscussionsController < ApplicationController
  layout 'admin_application'
  before_action :set_discussion, only: [:show, :edit, :update, :destroy]

  def index
    @discussions = Discussion.all
  end

  def show
  end

  def new
    @discussion = Discussion.new
  end

  def edit
  end

  def create
    @discussion = Discussion.new(discussion_params)

    if @discussion.save
      redirect_to polymorphic_path([:admin, @discussion]), notice: 'Discussion was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @discussion.update(discussion_params)
      redirect_to polymorphic_path([:admin, @discussion]), notice: 'Discussion was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @discussion.destroy
    redirect_to polymorphic_path([:admin, :discussions]), notice: 'Discussion was successfully destroyed.'
  end

  private
    def set_discussion
      @discussion = Discussion.find params[:id]
    end

    def discussion_params
      params[:discussion]
      params.require(:discussion).permit(:description, :title, :room_id, :short)
    end
end
