class PostsController < ApplicationController
  # before_filter :authenticate_user!
  before_filter :find_discussion
  before_filter :setup_negative_captcha, only: [:new, :create]

  def create
    # binding.pry
    post = @discussion.posts.new post_params
    post.user = current_or_guest_user
    post.save 

    redirect_to discussion_path(@discussion, page: last_page(@discussion.posts.count, 5))
  end

  private 

  def find_discussion
    @discussion = Discussion.find params[:discussion_id]
  end

  def post_params
    params.require(:post).permit(:content, :discussion_id, :user_id)
  end

  private
  def setup_negative_captcha
    @captcha = NegativeCaptcha.new(
      # A secret key entered in environment.rb. 'rake secret' will give you a good one.
      secret: NEGATIVE_CAPTCHA_SECRET,
      spinner: request.remote_ip, 
      # Whatever fields are in your form
      fields: [:name, :email, :body],  
      params: params
    )
  end
end
