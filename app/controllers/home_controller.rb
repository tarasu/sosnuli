class HomeController < ApplicationController
  # before_action :role_required,  only: [:index]
  before_action :login_required, except: [:index, :show]
  before_action :role_required,  except: [:index, :show]

  before_action :set_page,       only: [:edit, :update, :destroy]
  before_action :owner_required, only: [:edit, :update, :destroy]

  def index
    @room = Room.find('evromaidan')
    # @discussions = @room.discussions.page(params[:disc_page]).per(9)
    @discussions = @room.discussions.page(params[:page]).per(12)
    # @videos = @room.videos.page(params[:vi_page]).per(9)

    respond_to do |format|
      format.html
      format.js # add this line for your js template
    end


  end
end
