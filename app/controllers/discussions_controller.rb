class DiscussionsController < ApplicationController
  def index
    @discussions = Discussion.page(params[:page]).per(12)
  end

  def show
    @discussion = Discussion.find(params[:id])
    @posts = @discussion.posts.page(params[:page]).per(5)
  end

  def edit
    @discussions = Discussion.find params[:id]
  end
end
