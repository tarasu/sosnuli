class ApplicationController < ActionController::Base
  include TheRole::Controller
  
  protect_from_forgery

  helper_method :current_or_guest_user

  def current_or_guest_user
    if current_user
      if session[:guest_user_id]
        logging_in
        guest_user.destroy
        session[:guest_user_id] = nil
      end
      current_user
    else
      guest_user
    end
  end

  def guest_user
    # Cache the value the first time it's gotten.
    @cached_guest_user ||= User.find(session[:guest_user_id] ||= create_guest_user.id)

  rescue ActiveRecord::RecordNotFound # if session[:guest_user_id] invalid
     session[:guest_user_id] = nil
     guest_user
  end

  def access_denied
    flash[:error] = t('the_role.access_denied')
    redirect_to(:back)
  end

  private 
  def last_page count, per
    page = count / per
    count % per == 0 ? page : (page + 1) 
  end

  def create_guest_user
    id = "#{Time.now.to_i}#{rand(99)}#{rand(99)}"[8..-1]
    u = User.create(name: id, email: "guest_#{id}@sosnu.li")
    u.save!(validate: false)
    session[:guest_user_id] = u.id
    u
  end
end
