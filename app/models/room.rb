class Room < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  has_many :discussions, dependent: :destroy
  has_many :videos, dependent: :destroy
end
