class Video < ActiveRecord::Base
  belongs_to :user
  belongs_to :room

  validates :content,   presence: true
  validates :title,     presence: true
  validates :room_id,   presence: true
end
