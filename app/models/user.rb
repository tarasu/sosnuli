class User < ActiveRecord::Base
  include TheRole::User

  has_many :videos
  has_many :posts
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable#, :omniauthable

  before_create :default_name


  def self.find_for_vkontakte_oauth access_token
    if user = User.where(url: access_token.info.urls.Vkontakte).first
      user
    else 
      User.create!(provider: access_token.provider, url: access_token.info.urls.Vkontakte, 
                   name: access_token.info.name,
                   email: access_token.extra.raw_info.domain+'<hh user=vk>.com', 
                   password: Devise.friendly_token[0,20]) 
    end
  end


  private 
  def default_name
    self.name = "Guest #{rand(9999)}"
  end
end
