class CreateDiscussions < ActiveRecord::Migration
  def change
    create_table :discussions do |t|
      t.references :room, index: true
      t.string :title

      t.timestamps
    end
  end
end
