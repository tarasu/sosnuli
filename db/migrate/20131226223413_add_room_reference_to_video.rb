class AddRoomReferenceToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :room_id, :integer
    add_index  :videos, :room_id
  end
end
