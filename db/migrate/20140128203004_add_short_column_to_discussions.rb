class AddShortColumnToDiscussions < ActiveRecord::Migration
  def change
    add_column :discussions, :short, :text
  end
end
