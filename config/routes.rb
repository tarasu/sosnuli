Sosnuli::Application.routes.draw do

  get "omniauth_callbacks/facebook"
  get "omniauth_callbacks/vkontakte"
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  root to:'home#index'

  resources :discussions do
    resources :posts
  end
  
  resources :videos, only: [:show]

  get :admin, to: 'admin#index'
  namespace :admin do
    resources :videos
    resources :discussions
  end
end
