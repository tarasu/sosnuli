TheRole.configure do |config|
  config.layout            = :application # default Layout for TheRole UI
  config.default_user_role = nil          # set default role (name)
  config.access_denied_method  = :access_denied      # define it in ApplicationController
  config.login_required_method = :authenticate_user! # devise auth method

end