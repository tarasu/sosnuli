load "deploy/assets"

require 'bundler/capistrano'
require 'rvm/capistrano'

set :rvm_type,        :system
set :rvm_ruby_string, 'ruby-2.0.0-p353@sosnuli'

set :application, 'sosnuli'
set :rails_env, :production
set :domain, '95.85.28.242'

set :scm, :git
set :repository,  'git@bitbucket.org:tarasu/sosnuli.git'
set :branch,      'master'

ssh_options[:forward_agent] = true
default_run_options[:pty] = true

set :use_sudo,      false
set :user,          'root' # username on the server
set :password,      'qvhddsnukhgz'
set :deploy_to,     "/var/www/#{application}"
set :shared_files,  %w(config/database.yml)
set :keep_releases, 5
set :ssh_options,   { forward_agent: true }
set :linked_dirs, %w{log}

server '95.85.28.242', :app, :web, :db, primary: true

# to use new assets approach
# set :normalize_asset_timestamps, false

# unicorn related
set :unicorn_conf, "#{deploy_to}/current/config/unicorn.rb"
set :unicorn_pid,  "#{deploy_to}/shared/pids/unicorn.pid"

after "deploy:finalize_update", "deploy:update_shared_symlinks"

before "deploy:assets:symlink" do
  run "ln -nfs #{shared_path}/config/database.yml #{current_release}/config/database.yml"
end

namespace :deploy do
  after 'deploy', 'deploy:cleanup'

  desc 'Zero-downtime restart of Unicorn'
  task :restart do
    run "sudo /etc/init.d/unicorn_sosnuli restart"
  end

  task :start do
    run "sudo /etc/init.d/unicorn_sosnuli start"
  end

  task :stop do
    run "sudo /etc/init.d/unicorn_sosnuli stop"
  end

  task :update_shared_symlinks do
    shared_files.each do |path|
      run "ln -nfs #{File.join(deploy_to, "shared", path)} #{File.join(release_path, path)}"
    end
  end

  # Precompile assets only when needed
  namespace :assets do
    task :precompile, :roles => :web, :except => { :no_release => true } do
      # If this is our first deploy - don't check for the previous version
      if remote_file_exists?(current_path)
        from = source.next_revision(current_revision)
        if capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
          run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
        else
          logger.info "Skipping asset pre-compilation because there were no asset changes"
        end
      else
        run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
      end
    end
  end

  # Helper function
  def remote_file_exists?(full_path)
    'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
  end

  desc "tail production log files"
  task :tail_logs, :roles => :app do
    run "tail -f #{shared_path}/log/production.log" do |channel, stream, data|
      trap("INT") { puts 'Interupted'; exit 0; }
      puts  # for an extra line break before the host name
      puts "#{channel[:host]}: #{data}"
      break if stream == :err
    end
  end

  desc "reload the database with seed data"
  task :seed do
    run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  end
end
